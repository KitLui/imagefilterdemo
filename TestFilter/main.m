//
//  main.m
//  TestFilter
//
//  Created by Four Directions on 5/10/15.
//  Copyright © 2015 Four Directions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
