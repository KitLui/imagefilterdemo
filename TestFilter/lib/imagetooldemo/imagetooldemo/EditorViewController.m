//
//  EditorViewController.m
//  imagetool
//
//  Created by benson on 4/9/13.
//  Copyright (c) 2013 appgreen. All rights reserved.
//

#import "EditorViewController.h"
#import "ImageToolManager.h"

@interface EditorViewController ()
- (void)reset;
- (void)renderImage:(int)effect type:(int)type;
- (void)triggerControl:(id)sender;
- (void)triggerExport:(id)sender;
@end

@implementation EditorViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithEffect:(int)effect
{
    self = [super initWithNibName:nil bundle:nil];
    if(self)
    {
        _effect = effect;
        
        typeControl = [[UISegmentedControl alloc] init];
        [typeControl setSegmentedControlStyle:UISegmentedControlStylePlain];
        [typeControl setSelectedSegmentIndex:0];
        [typeControl setTintColor:[UIColor grayColor]];
        [typeControl addTarget:self action:@selector(triggerControl:) forControlEvents:UIControlEventValueChanged];
        [self.navigationItem setTitleView:typeControl];
        [typeControl release];
        
        if(_effect==3)
        {
            [typeControl insertSegmentWithTitle:@"Center" atIndex:0 animated:NO];
            [typeControl insertSegmentWithTitle:@"Horizontal" atIndex:1 animated:NO];
            [typeControl insertSegmentWithTitle:@"Vertical" atIndex:2 animated:NO];
            [typeControl setSelectedSegmentIndex:0];
        }
        else if(_effect==6)
        {
            [typeControl insertSegmentWithTitle:@"Red Wash" atIndex:0 animated:NO];
            [typeControl insertSegmentWithTitle:@"Cool Summer" atIndex:1 animated:NO];
            [typeControl setSelectedSegmentIndex:0];
        }
        
        UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Export" style:UIBarButtonItemStyleDone target:self action:@selector(triggerExport:)];
        [self.navigationItem setRightBarButtonItem:rightBarButton];
        [rightBarButton release];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [imageView setBackgroundColor:[UIColor clearColor]];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    [imageView setImage:nil];
    [self.view addSubview:imageView];
    [imageView release];
    
    [self renderImage:_effect type:0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reset
{
    [imageView setImage:nil];
}

- (void)renderImage:(int)effect type:(int)type
{
    [self reset];
    
    UIImage *originalImage = [[ImageToolManager sharedManager] resizeImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"original_1" ofType:@"png"]] size:TARGET_SIZE];
    UIImage *resultImage = nil;
    
    NSError *error = nil;
    switch (_effect) {
        case 0:
        {
            resultImage = originalImage;
            break;
        }
        case 1:
        {
            UIImage *maskImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"theme_option_original_12" ofType:@"png"]];
            resultImage = [[ImageToolManager sharedManager] cropImage:originalImage inRect:self.view.frame withImage:maskImage inRect:self.view.frame];
            break;
        }
        case 2:
        {
            UIImage *layerImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"theme_option_original_6" ofType:@"png"]];
            resultImage = [[ImageToolManager sharedManager] addLayer:originalImage inRect:self.view.frame withImage:layerImage inRect:self.view.frame];
            break;
        }
        case 3:
        {
            if(type==1)
                
                resultImage = [[ImageToolManager sharedManager] tiltShiftHorizontally:originalImage url:nil realTime:YES inputRadius:10 inputCenter:0.5 inputTop:0.75 inputBottom:0.25 error:&error];
            else if(type==2)
                resultImage = [[ImageToolManager sharedManager] tiltShiftVertically:originalImage url:nil realTime:YES inputRadius:10 inputCenter:0.5 inputLeft:0.25 inputRight:0.75 error:&error];
            else
                resultImage = [[ImageToolManager sharedManager] tiltShiftCenter:originalImage url:nil realTime:YES inputRadius:10 inputCenter:0.5 error:&error];
            break;
        }
        case 4:
        {
            resultImage = [[ImageToolManager sharedManager] sepia:originalImage url:nil realTime:YES intensity:0.8 error:&error];
            break;
        }
        case 5:
        {
            resultImage = [[ImageToolManager sharedManager] vignette:originalImage url:nil realTime:YES saturation:3.0 error:&error];
            break;
        }
        case 6:
        {
            if(type==1)
                resultImage = [[ImageToolManager sharedManager] colorMatrix:originalImage url:nil realTime:NO rVector:nil gVector:[CIVector vectorWithX:1 Y:1 Z:1 W:0.1] bVector:nil error:&error];
            else
                resultImage = [[ImageToolManager sharedManager] colorMatrix:originalImage url:nil realTime:NO rVector:[CIVector vectorWithX:1 Y:1 Z:1 W:0.1] gVector:nil bVector:nil error:&error];
            break;
        }
        case 7:
        {
            resultImage = [[ImageToolManager sharedManager] pixellate:originalImage url:nil realTime:YES andScale:10.0f error:&error];
            break;
        }
        case 8:
        {
            resultImage = [[ImageToolManager sharedManager] grayScale:originalImage url:nil error:&error];
            break;
        }
        case 9:
        {
            resultImage = [[ImageToolManager sharedManager] generateNoise:originalImage url:nil realTime:YES error:&error];
            break;
        }
        default:
            break;
    }
    if(error)
        NSLog(@"%@", error);
    
    [imageView setImage:resultImage];
}

- (void)triggerControl:(id)sender
{
    [self renderImage:_effect type:typeControl.selectedSegmentIndex];
}

- (void)triggerExport:(id)sender
{
    if(!imageView.image)
        return;
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *saveDir = [paths objectAtIndex:0];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:saveDir withIntermediateDirectories:YES attributes:nil error:nil];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"YYYYMMddHHmmss"];
    NSString *dateString = [outputFormatter stringFromDate:[NSDate date]];
    [outputFormatter release];
    
    NSString *savePath = [saveDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_%@_%d", dateString, effectName[_effect], typeControl.selectedSegmentIndex]];
    NSData *imageData = UIImagePNGRepresentation(imageView.image);
    
    BOOL result = [[NSFileManager defaultManager] createFileAtPath:savePath contents:imageData attributes:nil];
    if(!result)
        NSLog(@"Failed to save");
}

@end
