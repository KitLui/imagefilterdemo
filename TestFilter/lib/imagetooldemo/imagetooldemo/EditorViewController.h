//
//  EditorViewController.h
//  imagetool
//
//  Created by benson on 4/9/13.
//  Copyright (c) 2013 appgreen. All rights reserved.
//

#import "RootViewController.h"

@interface EditorViewController : RootViewController
{
    int _effect;
    
    UIImageView *imageView;
    UISegmentedControl *typeControl;
}

- (id)initWithEffect:(int)effect;

@end
