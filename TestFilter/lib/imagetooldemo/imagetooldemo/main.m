//
//  main.m
//  imagetooldemo
//
//  Created by benson on 19/9/13.
//  Copyright (c) 2013 appgreen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
