//
//  ImageToolManager.m
//  imagetool
//
//  Created by benson on 4/9/13.
//  Copyright (c) 2013 appgreen. All rights reserved.
//

#import "ImageToolManager.h"
#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/EAGL.h>
#import "UIImage+Resize.h"

static ImageToolManager *sharedManager = nil;
NSString *effectName[NUM_OF_EFFECT] = {@"Original", @"Crop", @"Layer", @"Tilt-Shift", @"Sepia", @"Vignette", @"Color Matrix", @"Pixellate", @"GrayScale", @"Noise"};

@implementation ImageToolManager

+ (ImageToolManager* ) sharedManager
{
    @synchronized(sharedManager)
    {
        if(sharedManager == nil)
        {
            sharedManager = [[ImageToolManager  alloc] init];
        }
    }
    return sharedManager;
}

+ (void) releaseSharedManager
{
    @synchronized(sharedManager)
    {
        if(sharedManager != nil)
        {
            [sharedManager release];
            sharedManager = nil;
        }
    }
}

- (NSError*)getError:(int)code
{
    switch (code)
    {
        case ERROR_INVALID_ORIGINAL_IMAGE_CODE:
            return [NSError errorWithDomain:@"invalid original image" code:code userInfo:nil];
        case ERROR_UNSUPPORT_FILTER_CODE:
            return [NSError errorWithDomain:@"unsupport filter" code:code userInfo:nil];
        default:
            break;
    }
    return nil;
}

- (UIImage*)fixOrientation:(UIImage*)image
{
    if (image.imageOrientation == UIImageOrientationUp) return image;
   
    CGSize imageSize = CGSizeMake(image.size.width, image.size.height);
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (image.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, imageSize.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, imageSize.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (image.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, imageSize.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, imageSize.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, imageSize.width, imageSize.height,
                                             CGImageGetBitsPerComponent(image.CGImage), 0,
                                             CGImageGetColorSpace(image.CGImage),
                                             CGImageGetBitmapInfo(image.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (image.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            CGContextDrawImage(ctx, CGRectMake(0,0,imageSize.height,imageSize.width), image.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,imageSize.width,imageSize.height), image.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgImage = CGBitmapContextCreateImage(ctx);
    UIImage *resultImage = [UIImage imageWithCGImage:cgImage];
    CGContextRelease(ctx);
    CGImageRelease(cgImage);
    
    return resultImage;
}

- (UIImage*)resizeImage:(UIImage*)image size:(CGSize)size
{
    return [image resizedImageToFitInSize:size scaleIfSmaller:YES];
}

- (UIImage*)cropImage:(UIImage*)image inRect:(CGRect)imageFrame withImage:(UIImage*)maskImage inRect:(CGRect)maskImageFrame
{
    if(!image)
        return nil;
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGImageRef maskImageRef = [maskImage CGImage];
    CGContextRef context = CGBitmapContextCreate (NULL, imageFrame.size.width, imageFrame.size.height, 8, 0, colorSpace, kCGImageAlphaPremultipliedLast);
    
    CGColorSpaceRelease(colorSpace);
    
    if (context==NULL)
        return nil;
    
    CGFloat ratio = maskImageFrame.size.width / maskImage.size.width;
    if(maskImageFrame.size.height < ratio * maskImage.size.height) {
        ratio = maskImageFrame.size.height / maskImage.size.height;
    }
    CGRect maskRect  = {{maskImageFrame.origin.x-imageFrame.origin.x, imageFrame.size.height-maskImageFrame.size.height-(maskImageFrame.origin.y-imageFrame.origin.y)}, {ceil(maskImage.size.width*ratio), ceil(maskImage.size.height*ratio)}};
    CGRect imageRect = {{0, 0}, {imageFrame.size.width, imageFrame.size.height}};
    
    CGContextClipToMask(context, maskRect, maskImageRef);
    CGContextDrawImage(context, imageRect, image.CGImage);
    
    CGImageRef newImage = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    
    UIImage *resultImage = [UIImage imageWithCGImage:newImage];
    
    CGImageRelease(newImage);
    
    return resultImage;
}

- (UIImage*)addLayer:(UIImage*)image inRect:(CGRect)imageFrame withImage:(UIImage*)layerImage inRect:(CGRect)layerFrame
{
    UIGraphicsBeginImageContextWithOptions(imageFrame.size, NO, 0.0f);
    
    if(image)
        [image drawInRect:CGRectMake(0, 0, imageFrame.size.width, imageFrame.size.height)];
    
    CGFloat ratio = layerFrame.size.width / layerImage.size.width;
    if(layerFrame.size.height < ratio * layerImage.size.height) {
        ratio = layerFrame.size.height / layerImage.size.height;
    }
    
    CGRect layerRect  = {{layerFrame.origin.x-imageFrame.origin.x, layerFrame.origin.y-imageFrame.origin.y}, {ceil(layerImage.size.width*ratio), ceil(layerImage.size.height*ratio)}};
    
    [layerImage drawInRect:layerRect];
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resultImage;
}

- (UIImage*)tiltShiftHorizontally:(UIImage*)image url:(NSURL*)url realTime:(BOOL)realTime inputRadius:(CGFloat)inputRadius inputCenter:(CGFloat)inputCenter inputTop:(CGFloat)inputTop inputBottom:(CGFloat)inputBottom error:(NSError**)error
{
    return [self tiltShift:image url:url realTime:realTime inputRadius:inputRadius inputCenter:inputCenter inputTop:inputTop inputBottom:inputBottom inputLeft:-1 inputRight:-1 error:error];
}

- (UIImage*)tiltShiftVertically:(UIImage*)image url:(NSURL*)url realTime:(BOOL)realTime inputRadius:(CGFloat)inputRadius inputCenter:(CGFloat)inputCenter inputLeft:(CGFloat)inputLeft inputRight:(CGFloat)inputRight error:(NSError**)error
{
    return [self tiltShift:image url:url realTime:realTime inputRadius:inputRadius inputCenter:inputCenter inputTop:-1 inputBottom:-1 inputLeft:inputLeft inputRight:inputRight error:error];
}

- (UIImage*)tiltShiftCenter:(UIImage*)image url:(NSURL*)url realTime:(BOOL)realTime inputRadius:(CGFloat)inputRadius inputCenter:(CGFloat)inputCenter error:(NSError**)error
{
    CIImage *ciImage = nil;
    if(image)
        ciImage = [CIImage imageWithCGImage:image.CGImage];
    else if(url)
        ciImage = [CIImage imageWithContentsOfURL:url];
    else
    {
        if(error)
            *error = [self getError:ERROR_INVALID_ORIGINAL_IMAGE_CODE];
        return nil;
    }
    
    CGRect cropRect = ciImage.extent;
    CIFilter *blur = [CIFilter filterWithName:@"CIGaussianBlur"
                                keysAndValues:@"inputImage", ciImage,
                      @"inputRadius", [NSNumber numberWithFloat:inputRadius], nil];
    if(!blur)
    {
        if (error)
            *error = [self getError:ERROR_UNSUPPORT_FILTER_CODE];
        return nil;
    }
    
    blur = [CIFilter filterWithName:@"CICrop"
                      keysAndValues:@"inputImage", blur.outputImage,
            @"inputRectangle", [CIVector vectorWithCGRect:cropRect], nil];
    
    CIVector *cen = [CIVector vectorWithX:cropRect.origin.x+cropRect.size.width*inputCenter Y:cropRect.origin.y+cropRect.size.height*inputCenter];
    CGFloat radius = MIN(cropRect.size.width, cropRect.size.height)/1.5;
    CIFilter *radialGradient = [CIFilter filterWithName:@"CIRadialGradient"
                                          keysAndValues:
                                @"inputRadius0", [NSNumber numberWithFloat:radius],
                                @"inputRadius1", [NSNumber numberWithFloat:inputRadius],
                                @"inputColor0", [CIColor colorWithRed:0.0 green:1.0 blue:0.0 alpha:1.0],
                                @"inputColor1", [CIColor colorWithRed:0.0 green:1.0 blue:0.0 alpha:0.0],
                                @"inputCenter", cen, nil];
    CIImage *circleImage = [radialGradient valueForKey:kCIOutputImageKey];
    CIFilter *tiltShift = [CIFilter filterWithName:@"CIBlendWithMask"
                                     keysAndValues:@"inputImage", blur.outputImage,
                           @"inputBackgroundImage", ciImage,
                           @"inputMaskImage", circleImage, nil];
    
    CIContext *context = nil;
    EAGLContext *eaglContext = nil;
    if(realTime)
    {
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        context = [CIContext contextWithEAGLContext:eaglContext];
    }
    else
        context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage:tiltShift.outputImage fromRect:[ciImage extent]];
    UIImage *resultImage = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    
    if(eaglContext)
    {
        [eaglContext release];
        eaglContext = nil;
    }
    
    return resultImage;
}

- (UIImage*)tiltShift:(UIImage*)image url:(NSURL*)url realTime:(BOOL)realTime inputRadius:(CGFloat)inputRadius inputCenter:(CGFloat)inputCenter inputTop:(CGFloat)inputTop inputBottom:(CGFloat)inputBottom inputLeft:(CGFloat)inputLeft inputRight:(CGFloat)inputRight error:(NSError**)error
{
    if(inputTop<0 && inputBottom<0 && inputLeft<0 && inputRight<0)
        return image;
    
    CIImage *ciImage = nil;
    if(image)
        ciImage = [CIImage imageWithCGImage:image.CGImage];
    else if(url)
        ciImage = [CIImage imageWithContentsOfURL:url];
    else
    {
        if(error)
            *error = [self getError:ERROR_INVALID_ORIGINAL_IMAGE_CODE];
        return nil;
    }
    
    CGRect cropRect = ciImage.extent;
    CGFloat width = cropRect.size.width;
    CGFloat height = cropRect.size.height;
    
    CIFilter *blur = [CIFilter filterWithName:@"CIGaussianBlur"
                                keysAndValues:@"inputImage", ciImage,
                      @"inputRadius", [NSNumber numberWithFloat:inputRadius], nil];
    
    if(!blur)
    {
        if (error)
            *error = [self getError:ERROR_UNSUPPORT_FILTER_CODE];
        return nil;
    }
    
    blur = [CIFilter filterWithName:@"CICrop"
                      keysAndValues:@"inputImage", blur.outputImage,
            @"inputRectangle", [CIVector vectorWithCGRect:cropRect], nil];
    
    CIFilter *topGradient = nil;
    CIFilter *bottomGradient = nil;
    CIFilter *leftGradient = nil;
    CIFilter *rightGradient = nil;
    
    CIFilter *gradients = nil;
    CIFilter *verticalGradients = nil;
    CIFilter *horizontalGradients = nil;
    
    if(inputTop>0 && inputBottom>0)
    {
        topGradient = [CIFilter filterWithName:@"CILinearGradient"
                                 keysAndValues:@"inputPoint0", [CIVector vectorWithX:0 Y:(inputTop * height)],
                       @"inputColor0", [CIColor colorWithRed:0 green:1 blue:0 alpha:1],
                       @"inputPoint1", [CIVector vectorWithX:0 Y:(inputCenter * height)],
                       @"inputColor1", [CIColor colorWithRed:0 green:1 blue:0 alpha:0], nil];
        bottomGradient = [CIFilter filterWithName:@"CILinearGradient"
                                    keysAndValues:@"inputPoint0", [CIVector vectorWithX:0 Y:(inputBottom * height)],
                          @"inputColor0", [CIColor colorWithRed:0 green:1 blue:0 alpha:1],
                          @"inputPoint1", [CIVector vectorWithX:0 Y:(inputCenter * height)],
                          @"inputColor1", [CIColor colorWithRed:0 green:1 blue:0 alpha:0], nil];
        topGradient = [CIFilter filterWithName:@"CICrop"
                                 keysAndValues:@"inputImage", topGradient.outputImage,
                       @"inputRectangle", [CIVector vectorWithCGRect:cropRect], nil];
        bottomGradient = [CIFilter filterWithName:@"CICrop"
                                    keysAndValues:@"inputImage", bottomGradient.outputImage,
                          @"inputRectangle", [CIVector vectorWithCGRect:cropRect], nil];
        
        verticalGradients = [CIFilter filterWithName:@"CIAdditionCompositing"
                                       keysAndValues:@"inputImage", topGradient.outputImage,
                             @"inputBackgroundImage", bottomGradient.outputImage, nil];
    }
    
    if(inputLeft>0 && inputRight>0)
    {
        leftGradient = [CIFilter filterWithName:@"CILinearGradient"
                                            keysAndValues:@"inputPoint0", [CIVector vectorWithX:(inputLeft * width) Y:0],
                                  @"inputColor0", [CIColor colorWithRed:0 green:1 blue:0 alpha:1],
                                  @"inputPoint1", [CIVector vectorWithX:(inputCenter * width) Y:0],
                                  @"inputColor1", [CIColor colorWithRed:0 green:1 blue:0 alpha:0], nil];
        
        rightGradient = [CIFilter filterWithName:@"CILinearGradient"
                                             keysAndValues:@"inputPoint0", [CIVector vectorWithX:(inputRight * width) Y:0],
                                   @"inputColor0", [CIColor colorWithRed:0 green:1 blue:0 alpha:1],
                                   @"inputPoint1", [CIVector vectorWithX:(inputCenter * width) Y:0],
                                   @"inputColor1", [CIColor colorWithRed:0 green:1 blue:0 alpha:0], nil];
        
        leftGradient = [CIFilter filterWithName:@"CICrop"
                                  keysAndValues:@"inputImage", leftGradient.outputImage,
                        @"inputRectangle", [CIVector vectorWithCGRect:cropRect], nil];
        rightGradient = [CIFilter filterWithName:@"CICrop"
                                   keysAndValues:@"inputImage", rightGradient.outputImage,
                         @"inputRectangle", [CIVector vectorWithCGRect:cropRect], nil];
        
        horizontalGradients = [CIFilter filterWithName:@"CIAdditionCompositing"
                                                   keysAndValues:@"inputImage", leftGradient.outputImage,
                                         @"inputBackgroundImage", rightGradient.outputImage, nil];
    }
    
    if(verticalGradients)
        gradients = verticalGradients;
    else if(horizontalGradients)
        gradients = horizontalGradients;
    
    CIFilter *tiltShift = [CIFilter filterWithName:@"CIBlendWithMask"
                                     keysAndValues:@"inputImage", blur.outputImage,
                           @"inputBackgroundImage", ciImage,
                           @"inputMaskImage", gradients.outputImage, nil];
    
    CIContext *context = nil;
    EAGLContext *eaglContext = nil;
    if(realTime)
    {
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        context = [CIContext contextWithEAGLContext:eaglContext];
    }
    else
        context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage:tiltShift.outputImage fromRect:[ciImage extent]];
    UIImage *resultImage = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    
    if(eaglContext)
    {
        [eaglContext release];
        eaglContext = nil;
    }
    
    return resultImage;
}

- (UIImage*)sepia:(UIImage*)image url:(NSURL*)url realTime:(BOOL)realTime intensity:(CGFloat)intensity error:(NSError**)error
{
    CIImage *ciImage = nil;
    if(image)
        ciImage = [CIImage imageWithCGImage:image.CGImage];
    else if(url)
        ciImage = [CIImage imageWithContentsOfURL:url];
    else
    {
        if(error)
            *error = [self getError:ERROR_INVALID_ORIGINAL_IMAGE_CODE];
        return nil;
    }
    
    CIFilter *filter = [CIFilter filterWithName:@"CISepiaTone"
                            keysAndValues:@"inputImage", ciImage,
                        @"inputIntensity", [NSNumber numberWithFloat:intensity], nil];
    if(!filter)
    {
        if (error)
            *error = [self getError:ERROR_UNSUPPORT_FILTER_CODE];
        return nil;
    }
    CIContext *context = nil;
    EAGLContext *eaglContext = nil;
    if(realTime)
    {
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        context = [CIContext contextWithEAGLContext:eaglContext];
    }
    else
        context = [CIContext contextWithOptions:nil];
    
    CGImageRef cgImage = [context createCGImage:filter.outputImage fromRect:[ciImage extent]];
    UIImage *resultImage = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    
    if(eaglContext)
    {
        [eaglContext release];
        eaglContext = nil;
    }
    
    return resultImage;
}

- (UIImage*)vignette:(UIImage*)image url:(NSURL*)url realTime:(BOOL)realTime saturation:(CGFloat)saturation error:(NSError**)error
{
    CIImage *ciImage = nil;
    if(image)
        ciImage = [CIImage imageWithCGImage:image.CGImage];
    else if(url)
        ciImage = [CIImage imageWithContentsOfURL:url];
    else
    {
        if(error)
            *error = [self getError:ERROR_INVALID_ORIGINAL_IMAGE_CODE];
        return nil;
    }
    
    CIFilter *lighten = [CIFilter filterWithName:@"CIColorControls"
                         keysAndValues:@"inputImage", ciImage,
                         @"inputSaturation", [NSNumber numberWithFloat:saturation], nil];
    
    CIFilter *vignette = [CIFilter filterWithName:@"CIVignette"
                                  keysAndValues:@"inputImage", lighten.outputImage,
                        @"inputIntensity", [NSNumber numberWithFloat:1.0],
                        @"inputRadius", [NSNumber numberWithFloat:2.0], nil];
    if(!lighten || !vignette)
    {
        if (error)
            *error = [self getError:ERROR_UNSUPPORT_FILTER_CODE];
        return nil;
    }

    CIContext *context = nil;
    EAGLContext *eaglContext = nil;
    if(realTime)
    {
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        context = [CIContext contextWithEAGLContext:eaglContext];
    }
    else
        context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage:vignette.outputImage fromRect:[ciImage extent]];
    UIImage *resultImage = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    
    if(eaglContext)
    {
        [eaglContext release];
        eaglContext = nil;
    }
    
    return resultImage;
}

- (UIImage*)colorMatrix:(UIImage*)image url:(NSURL*)url realTime:(BOOL)realTime rVector:(CIVector*)rv gVector:(CIVector*)gv bVector:(CIVector*)bv error:(NSError**)error
{
    CIImage *ciImage = nil;
    if(image)
        ciImage = [CIImage imageWithCGImage:image.CGImage];
    else if(url)
        ciImage = [CIImage imageWithContentsOfURL:url];
    else
    {
        if(error)
            *error = [self getError:ERROR_INVALID_ORIGINAL_IMAGE_CODE];
        return nil;
    }
    
    CIFilter *filter = [CIFilter filterWithName:@"CIColorMatrix"];
    if(!filter)
    {
        if (error)
            *error = [self getError:ERROR_UNSUPPORT_FILTER_CODE];
        return nil;
    }
    
    [filter setDefaults];
    [filter setValue:ciImage forKey:kCIInputImageKey];
    if(rv)
        [filter setValue:rv forKey:@"inputRVector"];
    if(gv)
        [filter setValue:gv forKey:@"inputGVector"];
    if(bv)
        [filter setValue:bv forKey:@"inputBVector"];
    
    CIContext *context = nil;
    EAGLContext *eaglContext = nil;
    if(realTime)
    {
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        context = [CIContext contextWithEAGLContext:eaglContext];
    }
    else
        context = [CIContext contextWithOptions:nil];
    
    CGImageRef cgImage = [context createCGImage:filter.outputImage fromRect:[ciImage extent]];
    UIImage *resultImage = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    
    if(eaglContext)
    {
        [eaglContext release];
        eaglContext = nil;
    }
    
    return resultImage;
}

- (UIImage*)pixellate:(UIImage*)image url:(NSURL*)url realTime:(BOOL)realTime andScale:(float)scale error:(NSError**)error
{
    CIImage *ciImage = nil;
    if(image)
        ciImage = [CIImage imageWithCGImage:image.CGImage];
    else if(url)
        ciImage = [CIImage imageWithContentsOfURL:url];
    else
    {
        if(error)
            *error = [self getError:ERROR_INVALID_ORIGINAL_IMAGE_CODE];
        return nil;
    }
    
    CIFilter *filter = [CIFilter filterWithName:@"CIPixellate"];
    if(!filter)
    {
        if (error)
            *error = [self getError:ERROR_UNSUPPORT_FILTER_CODE];
        return nil;
    }
    
    [filter setDefaults];
    [filter setValue:ciImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithDouble:scale] forKey:@"inputScale"];
    
    CIContext *context = nil;
    EAGLContext *eaglContext = nil;
    if(realTime)
    {
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        context = [CIContext contextWithEAGLContext:eaglContext];
    }
    else
        context = [CIContext contextWithOptions:nil];
 
    CGImageRef cgImage = [context createCGImage:filter.outputImage fromRect:[ciImage extent]];
    UIImage *resultImage = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    
    if(eaglContext)
    {
        [eaglContext release];
        eaglContext = nil;
    }
    
    return resultImage;
}

- (UIImage*)grayScale:(UIImage*)image url:(NSURL*)url error:(NSError**)error
{
    UIImage *filteredImage = nil;
    if(image)
        filteredImage = image;
    else if(url)
        filteredImage = [UIImage imageWithContentsOfFile:url.absoluteString];
    else
    {
        if(error)
            *error = [self getError:ERROR_INVALID_ORIGINAL_IMAGE_CODE];
        return nil;
    }
    
    CGRect imageRect = CGRectMake(0, 0, filteredImage.size.width, filteredImage.size.height);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    
    CGContextRef context = CGBitmapContextCreate(nil, imageRect.size.width, imageRect.size.height, 8, 0, colorSpace, kCGImageAlphaNone);
    CGContextDrawImage(context, imageRect, [image CGImage]);
    
    CGImageRef cgImage = CGBitmapContextCreateImage(context);
    UIImage *resultImage = [UIImage imageWithCGImage:cgImage];
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    CFRelease(cgImage);
    
    return resultImage;
}

- (UIImage*)generateNoise:(UIImage*)image url:(NSURL*)url realTime:(BOOL)realTime error:(NSError**)error
{
    CIImage *ciImage = nil;
    if(image)
        ciImage = [CIImage imageWithCGImage:image.CGImage];
    else if(url)
        ciImage = [CIImage imageWithContentsOfURL:url];
    else
    {
        if(error)
            *error = [self getError:ERROR_INVALID_ORIGINAL_IMAGE_CODE];
        return nil;
    }
    
    CIFilter *noise = [CIFilter filterWithName:@"CIRandomGenerator"];
    if(!noise)
    {
        if (error)
            *error = [self getError:ERROR_UNSUPPORT_FILTER_CODE];
        return nil;
    }
    
    CIFilter *colorFilter = [CIFilter filterWithName:@"CIColorMatrix" keysAndValues:@"inputImage", noise.outputImage, @"inputRVector", [CIVector vectorWithX:0 Y:1 Z:0 W:0], @"inputGVector", [CIVector vectorWithX:0 Y:1 Z:0 W:0], @"inputBVector", [CIVector vectorWithX:0 Y:1 Z:0 W:0], @"inputBiasVector", [CIVector vectorWithX:0 Y:0 Z:0 W:0], nil];
    
    CIFilter *whiteSpeck = [CIFilter filterWithName:@"CISourceOverCompositing"
                                     keysAndValues:@"inputImage", colorFilter.outputImage,
                           @"inputBackgroundImage", ciImage, nil];
    
    CIContext *context = nil;
    EAGLContext *eaglContext = nil;
    if(realTime)
    {
        eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        context = [CIContext contextWithEAGLContext:eaglContext];
    }
    else
        context = [CIContext contextWithOptions:nil];
    
    CGImageRef cgImage = [context createCGImage:whiteSpeck.outputImage fromRect:[ciImage extent]];
    UIImage *resultImage = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    
    if(eaglContext)
    {
        [eaglContext release];
        eaglContext = nil;
    }
    
    return resultImage;
}

@end
