//
//  MenuViewController.h
//  imagetool
//
//  Created by benson on 4/9/13.
//  Copyright (c) 2013 appgreen. All rights reserved.
//

#import "RootViewController.h"

@interface MenuViewController : RootViewController <UITableViewDelegate, UITableViewDataSource>
{
    UITableView *menuTableView;
}
@end
