//
//  AppDelegate.h
//  imagetooldemo
//
//  Created by benson on 19/9/13.
//  Copyright (c) 2013 appgreen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
