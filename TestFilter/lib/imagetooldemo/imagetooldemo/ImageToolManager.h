//
//  ImageToolManager.h
//  imagetool
//
//  Created by benson on 4/9/13.
//  Copyright (c) 2013 appgreen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define NUM_OF_EFFECT 10
#define TARGET_SIZE CGSizeMake(1754, 2480)

#define ERROR_INVALID_ORIGINAL_IMAGE_CODE 1000
#define ERROR_UNSUPPORT_FILTER_CODE 1001

extern NSString *effectName[NUM_OF_EFFECT];

@interface ImageToolManager : NSObject

+ (ImageToolManager*)sharedManager;
+ (void) releaseSharedManager;

- (NSError*)getError:(int)code;

- (UIImage*)fixOrientation:(UIImage*)image;
- (UIImage*)resizeImage:(UIImage*)image size:(CGSize)size;
- (UIImage*)cropImage:(UIImage*)image inRect:(CGRect)imageFrame withImage:(UIImage*)maskImage inRect:(CGRect)maskImageFrame;
- (UIImage*)addLayer:(UIImage*)image inRect:(CGRect)imageFrame withImage:(UIImage*)layerImage inRect:(CGRect)layerFrame;
- (UIImage*)tiltShiftHorizontally:(UIImage*)originalImage url:(NSURL*)url realTime:(BOOL)realTime inputRadius:(CGFloat)inputRadius inputCenter:(CGFloat)inputCenter inputTop:(CGFloat)inputTop inputBottom:(CGFloat)inputBottom error:(NSError**)error;
- (UIImage*)tiltShiftVertically:(UIImage*)image url:(NSURL*)url realTime:(BOOL)realTime inputRadius:(CGFloat)inputRadius inputCenter:(CGFloat)inputCenter inputLeft:(CGFloat)inputLeft inputRight:(CGFloat)inputRight error:(NSError**)error;
- (UIImage*)tiltShiftCenter:(UIImage*)image url:(NSURL*)url realTime:(BOOL)realTime inputRadius:(CGFloat)inputRadius inputCenter:(CGFloat)inputCenter error:(NSError**)error;
- (UIImage*)tiltShift:(UIImage*)image url:(NSURL*)url realTime:(BOOL)realTime inputRadius:(CGFloat)inputRadius inputCenter:(CGFloat)inputCenter inputTop:(CGFloat)inputTop inputBottom:(CGFloat)inputBottom inputLeft:(CGFloat)inputLeft inputRight:(CGFloat)inputRight error:(NSError**)error;
- (UIImage*)sepia:(UIImage*)image url:(NSURL*)url realTime:(BOOL)realTime intensity:(CGFloat)intensity error:(NSError**)error;
- (UIImage*)vignette:(UIImage*)image url:(NSURL*)url realTime:(BOOL)realTime saturation:(CGFloat)saturation error:(NSError**)error;
- (UIImage*)colorMatrix:(UIImage*)image url:(NSURL*)url realTime:(BOOL)realTime rVector:(CIVector*)rv gVector:(CIVector*)gv bVector:(CIVector*)bv error:(NSError**)error;
- (UIImage*)pixellate:(UIImage*)image url:(NSURL*)url realTime:(BOOL)realTime andScale:(float)scale error:(NSError**)error;
- (UIImage*)grayScale:(UIImage*)image url:(NSURL*)url error:(NSError**)error;
- (UIImage*)generateNoise:(UIImage*)image url:(NSURL*)url realTime:(BOOL)realTime error:(NSError**)error;

@end
