//
//  ImageToolHelper.h
//  TestFilter
//
//  Created by Four Directions on 5/10/15.
//  Copyright © 2015 Four Directions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <ImageToolManager.h>

typedef enum SHThemeType
{
    kThemeType_Original = 0,
    kThemeType_Crop_Star,
    kThemeType_Crop_Love,
    kThemeType_Crop_Deer,
    kThemeType_Crop_Happy_Birthday,
    kThemeType_Crop_Two_Heart,
    kThemeType_Layer_Film,
    kThemeType_Crop_Square_And_Diamond,
    kThemeType_Crop_Flower,
    kThemeType_Crop_Double_Diamond,
    kThemeType_Layer_Many_Diamond,
    kThemeType_Crop_X_Mas,
    kThemeType_Crop_Circle_With_Lines,
    kTotalThemeType
}SHThemeType;

typedef enum SHFilterType{
    kFilterType_Original = 0,
    kFilterType_BlackWhite,
    kFilterType_RedWash,
    kFilterType_CoolSummer,
    kFilterType_Sepia,
    kFilterType_Vignette,
    kFilterType_TiltShiftCenter,
    kFilterType_TiltShiftHorizontal,
    kFilterType_8Bit,
    kFilterType_Noise,
    kTotalFilterType,
}SHFilterType;

@interface ImageToolHelper : NSObject

+ (UIImage *)resizeImage:(UIImage *)originalImage width:(CGFloat)width height:(CGFloat)height;
+ (UIImage*)applyTheme:(UIImage*)image imageRect:(CGRect)imageFrame maskRect:(CGRect)maskFrame andType:(SHThemeType)themeType preview:(BOOL)preview;
+ (UIImage*)applyFilter:(UIImage*)image andType:(SHFilterType)filterType;

@end
