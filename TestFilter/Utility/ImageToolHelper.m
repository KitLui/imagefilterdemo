//
//  ImageToolHelper.m
//  TestFilter
//
//  Created by Four Directions on 5/10/15.
//  Copyright © 2015 Four Directions. All rights reserved.
//

#import "ImageToolHelper.h"

@implementation ImageToolHelper

+ (UIImage *)resizeImage:(UIImage *)originalImage width:(CGFloat)width height:(CGFloat)height {
    return [[ImageToolManager sharedManager] resizeImage:originalImage size:CGSizeMake(width,height)];
}

+ (UIImage*)applyTheme:(UIImage*)image imageRect:(CGRect)imageFrame maskRect:(CGRect)maskFrame andType:(SHThemeType)themeType preview:(BOOL)preview
{
    if(!image)
        return nil;
    
    switch (themeType) {
        case kThemeType_Original:
            if(preview)
                return nil;
            else
                return image;
        case kThemeType_Crop_Star:
        case kThemeType_Crop_Love:
        case kThemeType_Crop_Deer:
        case kThemeType_Crop_Happy_Birthday:
        case kThemeType_Crop_Two_Heart:
        case kThemeType_Crop_Square_And_Diamond:
        case kThemeType_Crop_Flower:
        case kThemeType_Crop_Double_Diamond:
        case kThemeType_Crop_X_Mas:
        case kThemeType_Crop_Circle_With_Lines:
            if(preview)
                return [[ImageToolManager sharedManager] cropImage:image inRect:imageFrame withImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"theme_option_preview_%d", themeType] ofType:@"png"]] inRect:maskFrame];
            else
                return [[ImageToolManager sharedManager] cropImage:image inRect:imageFrame withImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"theme_option_original_%d", themeType] ofType:@"png"]] inRect:maskFrame];
        case kThemeType_Layer_Film:
        case kThemeType_Layer_Many_Diamond:
        {
            if(preview)
                return [[ImageToolManager sharedManager] addLayer:nil inRect:imageFrame withImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"theme_option_preview_%d", themeType] ofType:@"png"]] inRect:maskFrame];
            else
                return [[ImageToolManager sharedManager] addLayer:image inRect:imageFrame withImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"theme_option_original_%d", themeType] ofType:@"png"]] inRect:maskFrame];
        }
        default:
            break;
    }
    return image;
}

+ (UIImage*)applyFilter:(UIImage*)image andType:(SHFilterType)filterType
{
    if(!image)
        return nil;
    
    NSError *error = nil;
    UIImage *targetImage = nil;
    switch (filterType) {
        case kFilterType_Original:
            targetImage = image;
            break;
        case kFilterType_8Bit:
            targetImage = [[ImageToolManager sharedManager] pixellate:image url:nil realTime:NO andScale:10.0f error:&error];
            break;
        case kFilterType_BlackWhite:
            targetImage = [[ImageToolManager sharedManager] grayScale:image url:nil error:&error];
            break;
        case kFilterType_RedWash:
            targetImage = [[ImageToolManager sharedManager] colorMatrix:image url:nil realTime:NO rVector:[CIVector vectorWithX:1 Y:1 Z:1 W:0.1] gVector:nil bVector:nil error:&error];
            break;
        case kFilterType_CoolSummer:
            targetImage = [[ImageToolManager sharedManager] colorMatrix:image url:nil realTime:NO rVector:nil gVector:[CIVector vectorWithX:1 Y:1 Z:1 W:0.1] bVector:nil error:&error];
            break;
        case kFilterType_Sepia:
            targetImage = [[ImageToolManager sharedManager] sepia:image url:nil realTime:NO intensity:0.8 error:&error];
            break;
        case kFilterType_Vignette:
            targetImage = [[ImageToolManager sharedManager] vignette:image url:nil realTime:NO saturation:3.0 error:&error];
            break;
        case kFilterType_TiltShiftCenter:
            targetImage = [[ImageToolManager sharedManager] tiltShiftCenter:image url:nil realTime:NO inputRadius:10 inputCenter:0.5 error:&error];
            break;
        case kFilterType_TiltShiftHorizontal:
            targetImage = [[ImageToolManager sharedManager] tiltShiftHorizontally:image url:nil realTime:NO inputRadius:10 inputCenter:0.5 inputTop:0.75 inputBottom:0.25 error:&error];
            break;
        case kFilterType_Noise:
            targetImage = [[ImageToolManager sharedManager] generateNoise:image url:nil realTime:NO error:&error];
            break;
        default:
            break;
    }
    
    return (targetImage?targetImage:image);
}

@end
