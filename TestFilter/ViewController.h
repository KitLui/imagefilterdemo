//
//  ViewController.h
//  TestFilter
//
//  Created by Four Directions on 5/10/15.
//  Copyright © 2015 Four Directions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageToolHelper.h"

@interface ViewController : UIViewController
{
    __weak IBOutlet UIImageView *imageView;
}

@end

