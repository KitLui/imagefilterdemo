//
//  ViewController.m
//  TestFilter
//
//  Created by Four Directions on 5/10/15.
//  Copyright © 2015 Four Directions. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    UIImage *image = [UIImage imageNamed:@"test.jpeg"];
    UIImage *resizedImage = [ImageToolHelper resizeImage:image width:320.0f height:200.0f];
    //UIImage *newImage = [ImageToolHelper applyFilter:resizedImage andType:kFilterType_BlackWhite];
    UIImage *newImage = [ImageToolHelper applyTheme:resizedImage imageRect:CGRectMake(0, 0, 320, 200) maskRect:CGRectMake(20, 20, 280, 160) andType:kThemeType_Crop_Star preview:YES];
    imageView.image = newImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
